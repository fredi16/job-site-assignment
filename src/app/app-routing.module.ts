import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthSeekerGuard } from './services/guards/authSeeker.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CreateJobListingComponent } from './recruiter/create-job-listing/create-job-listing.component';
import { AuthRecruiterGuard } from './services/guards/authRecruiter.guard';
import { AuthLoggedInGuard } from './services/guards/authLoggedIn.guard';
import { RdashboardComponent } from './recruiter/rdashboard/rdashboard.component';
import { SdashboardComponent } from './seeker/sdashboard/sdashboard.component';
import { UpdateJobListingComponent } from './recruiter/update-job-listing/update-job-listing.component';
import { JobOfferDetailedViewComponent } from './seeker/job-offer-detailed-view/job-offer-detailed-view.component';
import { SeekerProfileComponent } from './seeker/seeker-profile/seeker-profile.component';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  },
  {path: "login", component: LoginComponent, canActivate: [AuthLoggedInGuard]},
  {path: "signup", component: SignupComponent, canActivate: [AuthLoggedInGuard]},
  {path: "jobs", component: SdashboardComponent, canActivate: [AuthSeekerGuard]},
  {path: "jobs/:id", component: JobOfferDetailedViewComponent, canActivate: [AuthSeekerGuard]},
  {path: "dashboard", component: RdashboardComponent, canActivate: [AuthRecruiterGuard]},
  {path: "updatejob/:id", component: UpdateJobListingComponent, canActivate: [AuthRecruiterGuard]},
  {path: "createjob", component: CreateJobListingComponent, canActivate: [AuthRecruiterGuard]},
  {path: "profile", component: SeekerProfileComponent},
  {path: "**", component: PageNotFoundComponent}
  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }