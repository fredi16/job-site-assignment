import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { passwordValidator } from './validators/password.validator';
import { passwordMatchValidator } from './validators/passwordMatch.validator';
import { MatChipInputEvent } from '@angular/material/chips';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';



class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, signUpForm: FormGroupDirective | NgForm | null): boolean {
    return !!control?.dirty && (control.value !== signUpForm?.value.password);
  }
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  errorMatcher = new CrossFieldErrorMatcher();

  // SKILL CHIPS
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  skills: string[] = ["Angular"];

  addSkill(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.skills.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  removeSkill(skill: string): void {
    const index = this.skills.indexOf(skill);

    if (index >= 0) {
      this.skills.splice(index, 1);
    }
  }
  // END SKILL CHIPS

  // form initilization 
  signUpForm!: FormGroup;
  secondFormGroup!: FormGroup;

  constructor(private fb: FormBuilder, private authService:AuthenticationService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    // form validation
    this.signUpForm = this.fb.group({
      username: ["", [Validators.required]],
      email: ["", [Validators.required]],
      password: ["", [Validators.required, passwordValidator(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]],
      cpassword: ["", [Validators.required]],
      type: ["", [Validators.required]]
    }, {
      // cross-field validation
      validator: passwordMatchValidator
    })

    this.secondFormGroup = this.fb.group({
      fullName: ['', Validators.required],
      profileTitle: ['', Validators.required],
      location: ['', Validators.required],
      bio: [''],
      skills: this.fb.array([""]),
      education: this.fb.array([]),
      workExperience: this.fb.array([])
    });

  }

  passwordValidator(form: FormGroup) {
    const condition = form.get('password')?.value !== form.get('cpassword')?.value;

    return condition ? { passwordsDoNotMatch: true} : null;
  }

  onSubmit(){
    if(!this.signUpForm.valid) return
    if(this.signUpForm.value.type === 'recruiter'){
      this.authService.SignUpRecruiter(this.signUpForm.value);
    }
    if(this.signUpForm.value.type === 'seeker'){
      this.secondFormGroup.value.skills = this.skills;
      this.authService.SignUpSeeker(this.signUpForm.value, this.secondFormGroup.value);
    }
    
    // this.signUpForm.reset();
    // Object.keys(this.signUpForm.controls).forEach(key => {
    //   this.signUpForm.get(key)?.setErrors(null);
    // });
  }

  AddEducation(){
    const education = this.fb.group({
      university: [],
      degree: [],
      graduation: []
    });
    this.educationForm.push(education);
  }

  DeleteEducation(i: number){
    this.educationForm.removeAt(i);
  }

  AddWorkExperience(){
    const workExperience = this.fb.group({
      company: [],
      position: [],
      period: []
    });
    this.workExperienceForm.push(workExperience);
  }

  DeleteWorkExperience(i: number){
    this.workExperienceForm.removeAt(i);
  }

  get userName(){
    return this.signUpForm.get('username')
  }
  get email(){
    return this.signUpForm.get('email')
  }
  get password(){
    return this.signUpForm.get('password')
  }
  get cpassword(){
    return this.signUpForm.get('cpassword')
  }
  get educationForm(){
    return this.secondFormGroup.get('education') as FormArray;
  }
  get workExperienceForm(){
    return this.secondFormGroup.get('workExperience') as FormArray;
  }

}
