import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  showFiller = false;
  username!: string;

  constructor(private router: Router, public service:AuthenticationService,) {
  }

  ngOnInit() {
  }

  Logout() {
    this.service.SignOut();
  };
}

