import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RecruiterService } from 'src/app/services/recruiter.service';

@Component({
  selector: 'app-create-job-listing',
  templateUrl: './create-job-listing.component.html',
  styleUrls: ['./create-job-listing.component.css']
})
export class CreateJobListingComponent implements OnInit {

  newJobForm!: FormGroup;

  constructor(private fb: FormBuilder, private recService:RecruiterService) { 
    this.newJobForm = this.fb.group({
      title: ["", [Validators.required]],
      description: ["", [Validators.required, Validators.maxLength(500)]]
    }, {
      // cross-field validation
    });

  }

  ngOnInit(): void {
    
  }

  OnSubmit(){
    if(!this.newJobForm.valid) return
    this.recService.CreateJobOffer(this.newJobForm.value);
    this.newJobForm.reset();
    Object.keys(this.newJobForm.controls).forEach(key => {
      this.newJobForm.get(key)?.setErrors(null);
    });
  }

  get Title(){
    return this.newJobForm.value.title
  }

}
