import { Component, OnInit } from '@angular/core';
import { RecruiterService } from 'src/app/services/recruiter.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

export interface JobListings {
  title: string;
  description: string;
  propertyId: string;
}

let ELEMENT_DATA: JobListings[] = [];

@Component({
  selector: 'app-rdashboard',
  templateUrl: './rdashboard.component.html',
  styleUrls: ['./rdashboard.component.css']
})
export class RdashboardComponent implements OnInit {

  displayedColumns: string[] = ['title', 'description', 'actions'];

  isLoadingResults = true;

  constructor(private recService: RecruiterService, public router: Router) { 
  }

  ngOnInit(): void {
    this.LoadData();
  }
  
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  LoadData(){
    this.recService.GetJobOffers()
    .subscribe(e => {
      ELEMENT_DATA = [];
      e.forEach(data => {
        ELEMENT_DATA.push({title: data.title, description: data.description, propertyId: data.propertyId})
      });
      this.isLoadingResults = false;
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    });
  }

  OnDelete(i: number){
    this.recService.DeleteJobOffer(ELEMENT_DATA[i].propertyId);
    this.isLoadingResults = true;
    this.LoadData();
  }

  OnEdit(i: number){
    this.router.navigate(['/updatejob', ELEMENT_DATA[i].propertyId]);
  }
}
