import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateJobListingComponent } from './create-job-listing/create-job-listing.component';
import { MaterialModule } from '../material/material.module';
import { RdashboardComponent } from './rdashboard/rdashboard.component';
import { UpdateJobListingComponent } from './update-job-listing/update-job-listing.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CreateJobListingComponent,
    RdashboardComponent,
    UpdateJobListingComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    CreateJobListingComponent
  ]
})
export class RecruiterModule { }
