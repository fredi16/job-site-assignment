import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RecruiterService } from 'src/app/services/recruiter.service';

@Component({
  selector: 'app-update-job-listing',
  templateUrl: './update-job-listing.component.html',
  styleUrls: ['./update-job-listing.component.css']
})
export class UpdateJobListingComponent implements OnInit {

  updateJobForm!: FormGroup;

  isLoadingResults = true;

  id = this.route.snapshot.paramMap.get('id')!;

  constructor(private fb: FormBuilder, private recService:RecruiterService, private route: ActivatedRoute) { 
    this.updateJobForm = this.fb.group({
      title: ["", [Validators.required]],
      description: ["", [Validators.required, Validators.maxLength(500)]]
    }, {
      // cross-field validation
    });
    this.LoadData();
  }

  ngOnInit(): void {
  }

  OnSubmit(){
    if(!this.updateJobForm.valid) return
    this.recService.UpdateJobOffer(this.updateJobForm.value, this.id);
    this.updateJobForm.reset();
    Object.keys(this.updateJobForm.controls).forEach(key => {
      this.updateJobForm.get(key)?.setErrors(null);
    });
  }

  LoadData(){
    this.recService.GetSingleJobOffer(this.id).subscribe(e => {
      this.updateJobForm.setValue({title: e?.title, description: e?.description});
      this.isLoadingResults = false;
    })
  }

}
