import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobOfferDetailedViewComponent } from './job-offer-detailed-view.component';

describe('JobOfferDetailedViewComponent', () => {
  let component: JobOfferDetailedViewComponent;
  let fixture: ComponentFixture<JobOfferDetailedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobOfferDetailedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobOfferDetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
