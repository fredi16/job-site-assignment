import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { jobListing } from 'src/app/services/interfaces/jobListing';
import { SeekerService } from 'src/app/services/seeker.service';

@Component({
  selector: 'app-job-offer-detailed-view',
  templateUrl: './job-offer-detailed-view.component.html',
  styleUrls: ['./job-offer-detailed-view.component.css']
})
export class JobOfferDetailedViewComponent implements OnInit {

  isLoadingResults = true;

  DATA!: jobListing;

  createdAt!: Date;

  isFavorite = false;

  favorites: string[] = [];

  id = this.route.snapshot.paramMap.get('id')!;

  constructor(public service: AuthenticationService, private seekerService: SeekerService, private route: ActivatedRoute, ) { 
  }

  ngOnInit(): void {
    this.LoadData();
    this.IsFavorite();
  }

  LoadData(){
    this.seekerService.GetSingleJobOffer(this.id).subscribe(e => {
      this.DATA = e!;
      this.createdAt = new Date(this.DATA.createdAt);
      this.isLoadingResults = false;
    });
  }

  IsFavorite(){
    this.seekerService.GetFavorites(localStorage.getItem('id')!).subscribe(e => {
      this.favorites = e!.favorites;
      for(let e of this.favorites){
        if(e === this.id){
          this.isFavorite = true;
          break
        }else{
          this.isFavorite = false;
        }
      }
    });
  }

  RemoveFavorite(){
    this.seekerService.RemoveFavorite(localStorage.getItem('id')!, this.id);
    this.isFavorite = false;
  }
  AddFavorite(){
    this.seekerService.AddFavorite(localStorage.getItem('id')!, this.id);
    this.isFavorite = true;
  }
  ApplyJob(){
    this.seekerService.ApplyJob(localStorage.getItem('id')!, this.id);
  }
}
