import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { jobListing } from 'src/app/services/interfaces/jobListing';

@Component({
  selector: 'app-job-offer-view',
  templateUrl: './job-offer-view.component.html',
  styleUrls: ['./job-offer-view.component.css']
})
export class JobOfferViewComponent implements OnInit {

  @Input() data!: jobListing;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  DetailedView(){
    this.router.navigate([`/jobs/${this.data.propertyId}`]);
  }

}
