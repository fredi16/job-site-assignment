import { Component, OnInit } from '@angular/core';
import { jobListing } from 'src/app/services/interfaces/jobListing';
import { SeekerService } from 'src/app/services/seeker.service';

@Component({
  selector: 'app-sdashboard',
  templateUrl: './sdashboard.component.html',
  styleUrls: ['./sdashboard.component.css']
})
export class SdashboardComponent implements OnInit {

  isLoadingResults = true;

  reqDATA: jobListing[] = [];
  DATA: jobListing[] = [];

  constructor(private seekerService: SeekerService) { }

  ngOnInit(): void {
    this.LoadData();
  }

  LoadData(){
    this.seekerService.GetJobOffers().subscribe(e => {
      e.forEach(data => {
        this.reqDATA.push(data);
      });
      this.DATA = this.reqDATA;
      this.isLoadingResults = false;
    })
  }

  OnSearchChange(searchInput: Event){
    let val = (searchInput.target as HTMLInputElement).value;
    this.DATA = this.reqDATA.filter(el => {
      return el.title.toLowerCase().includes(val) || el.description.toLowerCase().includes(val);
    });
  }
}
