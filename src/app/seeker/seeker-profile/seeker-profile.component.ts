import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { jobListing } from 'src/app/services/interfaces/jobListing';
import { profileInfo } from 'src/app/services/interfaces/profileInfo';
import { SeekerService } from 'src/app/services/seeker.service';

@Component({
  selector: 'app-seeker-profile',
  templateUrl: './seeker-profile.component.html',
  styleUrls: ['./seeker-profile.component.css']
})
export class SeekerProfileComponent implements OnInit {

  skills!: string[];

  isLoadingResults = true;

  showFavorites = false;

  showApplied = false;

  DATA!: profileInfo;

  favoriteJobs: jobListing[] = [];

  appliedJobs: jobListing[] = [];

  constructor(public service: AuthenticationService, private seekerService: SeekerService) { 
  }

  ngOnInit(): void {
    this.LoadData();
  }

  LoadData(){
    this.seekerService.GetProfileInfo(localStorage.getItem('id')!).subscribe( e => {
      this.DATA = e!;
      console.log(this.DATA);
      this.skills = this.DATA.skills!;
      this.isLoadingResults = false;
      this.seekerService.GetJobOffers().subscribe(e => {
        e.forEach(el => {
          if(this.DATA.favorites.includes(el.propertyId)){
            this.favoriteJobs.push(el);
          }
          if(this.DATA.appliedJobs.includes(el.propertyId)){
            this.appliedJobs.push(el);
          }
        });
      });
    });
  }
  ShowFavorites(){
    this.showFavorites = !this.showFavorites;

  }
  ShowApplied(){
    this.showApplied = !this.showApplied;
  }
}