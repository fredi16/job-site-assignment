import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeekerProfileComponent } from './seeker-profile/seeker-profile.component';
import { SdashboardComponent } from './sdashboard/sdashboard.component';
import { JobOfferViewComponent } from './job-offer-view/job-offer-view.component';
import { JobOfferDetailedViewComponent } from './job-offer-detailed-view/job-offer-detailed-view.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [
    SeekerProfileComponent,
    SdashboardComponent,
    JobOfferViewComponent,
    JobOfferDetailedViewComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    SeekerProfileComponent,
    SdashboardComponent,
    JobOfferViewComponent,
    JobOfferDetailedViewComponent
  ]
})
export class SeekerModule { }
