import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { newUserForm } from './interfaces/newUserForm';
import { AngularFirestore } from '@angular/fire/firestore';
import { userInfo } from './interfaces/userInfo';
import { seekerProfileDetails } from './interfaces/seekerProfileDetails';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  usersDb!: AngularFireList<any>;
  user!: AngularFireObject<any>;
  userInfo!: userInfo;
  // public currentUser!: Observable<firebaseUser.User>;

  constructor(public afStore:AngularFirestore, public db: AngularFireDatabase, public afAuth: AngularFireAuth, private router: Router, private snackBar: MatSnackBar) {
    // this.currentUser = this.afAuth.authState.pipe(filter((user: firebaseUser.User | null): user is firebaseUser.User => !!user));
    // this.currentUser.subscribe(x => this.uid = x.uid);
  }

  SignUpRecruiter(userData: newUserForm) {
    this.afAuth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then((result: any) => {
        let id = result.user.uid;
        this.afStore.collection('/accounts').doc(id).set({
          username: userData.username,
          email: userData.email,
          type: userData.type
          }).catch(error => {
            console.log(error);
        })
        this.snackBar.open("Account created", "Close");
        this.router.navigate(["/login"]);
    }).catch(e => {
      this.snackBar.open("Email already in use", "Close");
    })
  }

  SignUpSeeker(userData: newUserForm, seekerForm: seekerProfileDetails) {
    this.afAuth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then((result: any) => {
        let id = result.user.uid;
        this.afStore.collection('/accounts').doc(id).set({
          username: userData.username,
          email: userData.email,
          type: userData.type,
          fullName: seekerForm.fullName,
          profileTitle: seekerForm.profileTitle,
          location: seekerForm.location,
          bio: seekerForm.bio,
          skills: seekerForm.skills,
          education: seekerForm.education,
          workExperience: seekerForm.workExperience
        })
        this.snackBar.open("Account created", "Close");
        this.router.navigate(["/login"]);
      }).catch(error => {
        this.snackBar.open("Email already in use", "Close");
      });
    }

  SignIn(loginForm: any){
      this.afAuth.signInWithEmailAndPassword(loginForm.email, loginForm.password).then(result=>{
      let id = result.user?.uid;
      this.afStore
        .collection<userInfo>('/accounts')
        .doc(id)
        .valueChanges()
        .pipe(first())
        .subscribe((e) => {
          let info = {
            username: e?.username,
            type: e?.type
          };
          localStorage.setItem('user', JSON.stringify(info));
          localStorage.setItem('id', id as string)
          localStorage.setItem('isLoggedIn', 'true');
          let redirectURL = e?.type === 'seeker' ? 'jobs' : 'dashboard';
          this.router.navigate([redirectURL]);
          });
      }).catch(error => {
        this.snackBar.open("Invalid email or password", "Close");
      });
    }

    SignOut(){
      this.afAuth.signOut().then( () =>{
        localStorage.clear();
        this.router.navigate(["login"]);
      });
    }

    IsAuthenticated(){
      return !!localStorage.getItem('isLoggedIn');
    }

    GetUserInfo(){
      this.userInfo = JSON.parse(localStorage.getItem('user')!);
      return this.userInfo;
    }
}



