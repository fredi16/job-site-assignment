import { TestBed } from '@angular/core/testing';

import { AuthRecruiterGuard } from './authRecruiter.guard';

describe('AuthRecruiterGuard', () => {
  let guard: AuthRecruiterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthRecruiterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
