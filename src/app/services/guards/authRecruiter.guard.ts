import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthRecruiterGuard implements CanActivate {
  constructor(private router: Router){

  }

  canActivate(): boolean{
    if(!(!!localStorage.getItem('isLoggedIn'))){
      alert("You need to login first");
      this.router.navigate(["login"]);
      return false;
    }else if(JSON.parse(localStorage.getItem('user')!).type == "recruiter"){
      return true;
    }
    else{
      alert("permission denied")
      this.router.navigate(["jobs"]);
      return false;
    }
  }
}
