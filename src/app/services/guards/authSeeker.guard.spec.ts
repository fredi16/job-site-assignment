import { TestBed } from '@angular/core/testing';

import { AuthSeekerGuard } from './authSeeker.guard';

describe('AuthGuard', () => {
  let guard: AuthSeekerGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthSeekerGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
