import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthSeekerGuard implements CanActivate {

  constructor(private router: Router){

  }

  canActivate(): boolean{

    if(JSON.parse(localStorage.getItem('user')!).type == "seeker"){
      return true;
    }else{
      alert("permission denied")
      this.router.navigate(["login"]);
      return false;
    }
  }
}
