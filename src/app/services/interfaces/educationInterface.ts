export interface educationInterface {
    university?: string,
    degree?: string,
    graduation?: string
}