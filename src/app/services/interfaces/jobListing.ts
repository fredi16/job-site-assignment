export interface jobListing {
    title: string,
    description: string,
    createdAt: number,
    uid: string,
    creatorUsername: string,
    propertyId: string
}