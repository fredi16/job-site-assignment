import { seekerProfileDetails } from "./seekerProfileDetails";
import { userInfo } from "./userInfo";

export interface profileInfo extends userInfo, seekerProfileDetails {

} 