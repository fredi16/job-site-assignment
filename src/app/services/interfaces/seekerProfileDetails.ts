import { educationInterface } from "./educationInterface";
import { workExperienceInterface } from "./workExperienceInterface";

export interface seekerProfileDetails {
    fullName: string,
    profileTitle: string,
    location: string,
    bio?: string,
    skills?: string[],
    education?: educationInterface[],
    workExperience?: workExperienceInterface[]
}