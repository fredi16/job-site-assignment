export interface userInfo {
    username: string,
    email: string,
    favorites: string[],
    appliedJobs: string[],
    type: "recruiter" | "seeker"
}