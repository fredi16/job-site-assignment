export interface workExperienceInterface {
    company?: string,
    position?: string,
    period?: string
}