import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { jobListing } from './interfaces/jobListing';
import { newJobForm } from './interfaces/newJobForm';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RecruiterService {

  private readonly id = localStorage.getItem('id')!;
  private readonly username = JSON.parse(localStorage.getItem('user')!).username;

  constructor(public afStore:AngularFirestore, public db: AngularFireDatabase, private router: Router, private snackBar: MatSnackBar) {
  }

  CreateJobOffer(form: newJobForm){
    this.afStore.collection('/joboffers').doc().set({
      title: form.title,
      description: form.description,
      uid: this.id,
      createdAt: new Date().getTime(),
      creatorUsername: this.username,
    }).then(res => {
      this.snackBar.open("Job offer created successfully", "Close");
      this.router.navigate(['dashboard']);
    }).catch((error) => {
      console.log(error);
    });

  }
  UpdateJobOffer(form: newJobForm, id: string){
    this.afStore.collection('/joboffers').doc(id).update({
      title: form.title,
      description: form.description,
      uid: this.id
    }).then(res => {
      this.snackBar.open("Job offer updated successfully", "Close");
      this.router.navigate(['dashboard']);
    }).catch((error) => {
      console.log(error);
    });

  }

  GetJobOffers(){
    return this.afStore.collection<jobListing>('/joboffers', ref => ref.where('uid', '==', this.id)).valueChanges({ idField: 'propertyId' }).pipe(first())

  }

  GetSingleJobOffer(id: string){
    return this.afStore.collection<jobListing>('/joboffers').doc(id).valueChanges().pipe(first())
  }

  DeleteJobOffer(id: string){
    this.afStore.collection('/joboffers').doc(id).delete();
    this.snackBar.open("Job offer deleted successfully", "Close");
  }

}
