import { Injectable } from '@angular/core';
import * as firebase from 'firebase'
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { jobListing } from './interfaces/jobListing';
import { userInfo } from './interfaces/userInfo';
import { profileInfo } from './interfaces/profileInfo';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SeekerService {

  constructor(public afStore:AngularFirestore, public db: AngularFireDatabase, private router: Router, private snackBar: MatSnackBar) { }

  GetJobOffers(){
    return this.afStore.collection<jobListing>('/joboffers').valueChanges({ idField: 'propertyId' });
  }
  
  GetSingleJobOffer(id: string){
    return this.afStore.collection<jobListing>('/joboffers').doc(id).valueChanges().pipe(first())
  }

  AddFavorite(id: string, propertyId: string){
    return this.afStore.collection('/accounts').doc(id).update({
      favorites: firebase.default.firestore.FieldValue.arrayUnion(`${propertyId}`),
    }).then(e => {
      this.snackBar.open("Added to favorites", "Close");
    });
  }

  RemoveFavorite(id: string, propertyId: string){
    return this.afStore.collection('/accounts').doc(id).update({
      favorites: firebase.default.firestore.FieldValue.arrayRemove(`${propertyId}`),
    }).then(e => {
      this.snackBar.open("Removed from favorites", "Close");
    });
  }

  GetFavorites(id: string){
    return this.afStore.collection<userInfo>('/accounts').doc(id).valueChanges().pipe(first());
  }

  ApplyJob(id: string, propertyId: string){
    return this.afStore.collection('/accounts').doc(id).update({
      appliedJobs: firebase.default.firestore.FieldValue.arrayUnion(`${propertyId}`),
    }).then(e => {
      this.snackBar.open("Application sent", "Close");
    });
  }

  GetProfileInfo(id: string){
    return this.afStore.collection<profileInfo>('/accounts').doc(id).valueChanges().pipe(first())
  }

  GetAppliedJobs(id: string){
    return this.afStore.collection<userInfo>('/accounts').doc(id).valueChanges().pipe(first());
  }
}
