// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDsJeqOP7AaZPAACxceim51B1stQKKffgE",
    authDomain: "job-site-13a3f.firebaseapp.com",
    projectId: "job-site-13a3f",
    storageBucket: "job-site-13a3f.appspot.com",
    messagingSenderId: "826048225386",
    appId: "1:826048225386:web:c6b349bea763be67c66a13",
    measurementId: "G-2PH1XNZL62"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
